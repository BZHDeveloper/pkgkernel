#! /bin/bash

# Get icon
ICON=`pwd`/tux.png
# Get installed kernel version
CURRENT=`uname -r|cut -d'-' -f 1`
# Get last available stable version
INFO=`curl -Ls https://www.kernel.org/ | perl -lne 'BEGIN{$/=""} print "$1 $2" if \
    /latest_link.*?<a.*?href=\"(.*?)\".*?>(.*?)</s'`
LASTVERSION=`echo ${INFO} | cut -d' ' -f 2`

if [ ${CURRENT} != ${LASTVERSION} ] ; then
    notify-send -i ${ICON} "Kernel update" "Need version available (${LASTVERSION})"
fi
